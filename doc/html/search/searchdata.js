var indexSectionsWithContent =
{
  0: "abcdefimoprsuv~",
  1: "mv",
  2: "v",
  3: "abcdefimoprsv~",
  4: "os",
  5: "u"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "related",
  5: "pages"
};

var indexSectionLabels =
{
  0: "Todos",
  1: "Classes",
  2: "Arquivos",
  3: "Funções",
  4: "Amigas",
  5: "Páginas"
};

